## Requirement OS
- redis (https://redis.io/, https://github.com/microsoftarchive/redis/releases)
- php version 7.2+
- composer
- node version 14+

## Project Setting
 WebSocket using LARAVEL version 8.6 and VUE 2.6.12
- Git clone project
- Run composer install
- Run npm install
- Create file .env (cp .env.chat)
- Config database
- Run php artisan key:generate
- Run php artisan migrate
- Run php artisan db:seed
- Run php artisan optimize:clear
- Run npm run watch-poll

Run Test:
Mở 2 trình duyệt khác nhau tương ứng với 2 URL phía dưới.
Home: http://127.0.0.1:8000/
Test Event: http://127.0.0.1:8000/test-event
